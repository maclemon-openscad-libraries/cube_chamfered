# cube_chamfered

An OpenSCAD library that allows you to replace `cube()` with `cube_chamfered()`.

## Installation

Clone this repo into your [OpenSCAD Libraries directory](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries).

## Using `cube_chamfered`

To include the library into your project:

```scad
// Include library from standard location
use <cube_chamfered/cube_chamfered.scad>
```

You can use `cube_chamfered` as a drop in replacement for `cube()`. You can
control the amount of chamfer by passing `chamfer`.
If `chamfer` is not passed, the default is 10% of the cube length of [size].
When explicitly passing `chamfer = 0` we fall back to a standard `cube()`.

```scad
cube_chamfered(size = [1, 1, 1], chamfer = 0.1, center = false);
```






The following examples are also available in `examples.scad`.

---

```scad
cube_chamfered();
```

![A default chamfered cube](cube_chamfered.png)

A default chamfered cube, without passing any parameters.

---

```scad
cube_chamfered(chamfer=0.3);
```

![A cube with a 30% edge size chamfer.](cube_chamfered-chamfer=0.3.png)

A cube with a 30% edge size chamfer.

---

```scad
cube_chamfered(20, chamfer=1, center = true);
```

![A larger cube with smaller chamfer, also centered.](cube_chamfered-20.png)

A larger cube with smaller chamfer, also centered.
