// demo_cube_chamfered
// By MacLemon 2023

// Import the library which must be installed in a standard location.abs
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries
use <cube_chamfered/cube_chamfered.scad>;

// Quick previews, slower but high quality renders and .stl exports
$fn = $preview ? 16 : 64; 


// EXAMPLES
// Can be used as a drop-in replacement in the same way as a cube().
//%cube(); // just a standard cube, for comparison
cube_chamfered();

// This is what cube_chamfered defaults to when not passing any parameter, like above.
//cube_chamfered(size = [1, 1, 1], chamfer = 0.1, center = false);

// 30% chamfer
//cube_chamfered(chamfer=0.3);

// Cube with smaller chamfer, also centered
//cube_chamfered(20, chamfer=1, center = true);

// black monolith
//color("black") cube_chamfered(size = [1 , 4 ,9], chamfer = 0);