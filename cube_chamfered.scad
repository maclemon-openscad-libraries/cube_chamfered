edge = 1;
chamfer_scale = 1.3;
corner_scale = 1.0;

module pointy_cube_chamfered(side_length){
    intersection(){
        // just the cube that shall get chamfered edges
        cube([side_length, side_length, side_length], center = true);

        // Chamfer X edges
        rotate([45, 0, 0]){
            scale(chamfer_scale){
                cube([side_length, side_length, side_length], center = true);
            }
        }

        // Chamfer Y edges
        rotate([0, 45, 0]){
            scale(chamfer_scale){
                cube([side_length, side_length, side_length], center = true);
            }
        }

        // Chamfer Z edges
        rotate([0, 0, 45]){
            scale(chamfer_scale){
                cube([side_length, side_length, side_length], center = true);
            }
        }
    }
}

// Chamfered edges, but pointy corners
// usually not what people want.
//pointy_cube_chamfered(edge);

// ############################################
module octahedron_box(size) {
    cube([2*size, 2*size, size], center = true);
}

module octahedron(size) {
    dihedral = 109.47122;
    n = 3;
    intersection(){
        octahedron_box(size);
        intersection_for(i=[1:n]) {
            rotate([dihedral, 0, 360 /n * i]){
                octahedron_box(size);
            }
        }
    }
}

module pointy_octahedron(size){
    rotate([125.26, 180, 45]){
        octahedron(size);
    }
}
//pointy_octahedron(size = 1);
//pointy_octahedron(1);

// Creates chamfered edges and blunt corners.
// The size of the resulting cube does NOT have a length of "edge".
// Do not use this, it's just here for explanation.
/*
minkowski(){
    cube(edge, center=true);
    pointy_octahedron(0.1);
}
*/

// ############################################


// Creates rounded edges and corners
// The size of the resulting cube has a correct edge length of "size".
module roundedcube(size = [1, 1, 1], center = false, radius = 0.5, apply_to = "all") {
    $fn=20;
	// If single value, convert to [x, y, z] vector
	size = (size[0] == undef) ? [size, size, size] : size;

	translate_min = radius;
	translate_xmax = size[0] - radius;
	translate_ymax = size[1] - radius;
	translate_zmax = size[2] - radius;

	obj_translate = (center == false) ?
		[0, 0, 0] : [
			-(size[0] / 2),
			-(size[1] / 2),
			-(size[2] / 2)
		];

	translate(v = obj_translate) {
		hull() {
			for (translate_x = [translate_min, translate_xmax]) {
				x_at = (translate_x == translate_min) ? "min" : "max";
				for (translate_y = [translate_min, translate_ymax]) {
					y_at = (translate_y == translate_min) ? "min" : "max";
					for (translate_z = [translate_min, translate_zmax]) {
						z_at = (translate_z == translate_min) ? "min" : "max";

						translate(v = [translate_x, translate_y, translate_z])
                        sphere(size);
					}
				}
			}
		}
	}
}
//roundedcube(10, center = true);

// ###################################

module cube_chamfered(size = [1, 1, 1], chamfer = 0.1, center = false){
    // If single value (scalar), convert to [x, y, z] vector
	size = (size[0] == undef) ? [size, size, size] : size;
	xlen = size[0];
	ylen = size[1];
	zlen = size[2];

    // If chamfer is not passed, we default to 10% of the cube length of [size].
    chamfer = (chamfer == undef) ? len(size)*0.1 : chamfer;

    // size translation correction for octahedron size
    shift = chamfer * sin(120);

	obj_translate = (center == false) ?
		[   (xlen / 2),
			(ylen / 2),
			(zlen / 2) ]
        :
        [0, 0, 0];

    translate(v = obj_translate) {
        if (chamfer == 0) { // When explicitly passing chamfer = 0 we fall back to a cube()
            cube(size = [xlen, ylen, zlen], center = true); // center always true here, we handle that ourselves.
        } else {                
            hull(){
                for (translate_x = [- (xlen / 2 - shift), (xlen / 2 - shift)]) {
                    x_at = (translate_x == - (xlen / 2 - shift)) ? "min" : "max";
                    for (translate_y = [- (ylen / 2 - shift), (ylen / 2 - shift)]) {
                        y_at = (translate_y == - (ylen / 2 - shift)) ? "min" : "max";
                        for (translate_z = [- (zlen / 2 - shift), (zlen / 2 - shift)]) {
                            z_at = (translate_z == - (zlen / 2 - shift)) ? "min" : "max";
                            translate(v = [translate_x, translate_y, translate_z]){
                                pointy_octahedron(chamfer);
                            } // translate
                        } // translate_z
                    } // translate_y
                } // translate_x
            } //hull()
        } //else
    } // obj_translate
}


// EXAMPLES
// Can be used as a drop-in replacement in the same way as a cube().
//#cube();
//cube_chamfered();

// This is what cube_chamfered defaults to when not passing any parameter.
//cube_chamfered(size = [1, 1, 1], chamfer = 0.1, center = false);

// 30% chamfer
//cube_chamfered(chamfer=0.3);

// Cube with smaller chamfer, also centered
//cube_chamfered(20, chamfer=1, center = true);

// black monolith
//color("black") cube_chamfered(size = [1 , 4 ,9], chamfer = 0);
